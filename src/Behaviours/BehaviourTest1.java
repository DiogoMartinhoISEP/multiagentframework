/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Behaviours;

import jade.core.AID;
import jade.lang.acl.ACLMessage;

/**
 *
 * @author Diogo Martinho
 */
public class BehaviourTest1 extends jade.core.behaviours.OneShotBehaviour {

    ACLMessage msg = new ACLMessage(ACLMessage.INFORM);

    @Override
    public void action() {

        System.out.println("Agent 1 starting chat...");
        AID id = new AID();
        //sets ACL messages parameters
        msg = new ACLMessage(ACLMessage.INFORM);
        id.setName("Agent2@192.168.3.127:1099/JADE");
        msg.addReceiver(id);
        msg.setContent("Start Chat");
        

    }

    public ACLMessage getMessageFromBehaviour() {
        //performs action message to exeute behaviour
        action();
        //returns ACL Message
        return msg;
    }

}
