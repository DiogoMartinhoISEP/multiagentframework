/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Behaviours;

import jade.core.AID;
import jade.lang.acl.ACLMessage;

/**
 *
 * @author Diogo Martinho
 */
public class BehaviourTest3 extends jade.core.behaviours.Behaviour {

    /**
     * @return the sendmessage
     */
    public ACLMessage getSendmessage() {
        return sendmessage;
    }

    /**
     * @param sendmessage the sendmessage to set
     */
    public void setSendmessage(ACLMessage sendmessage) {
        this.sendmessage = sendmessage;
    }

    private ACLMessage receivedmessage;
    private ACLMessage sendmessage;

    @Override
    public void action() {

        System.out.println("Agent 2 receiving message..." + getReceivedmessage().getContent());
        AID id = new AID();
        //sets ACL messages parameters
        setSendmessage(new ACLMessage(ACLMessage.INFORM));

        id.setName("Agent1@192.168.3.127:1099/JADE");

        getSendmessage().addReceiver(id);

        getSendmessage().setContent("Hello 2");

    }

    public ACLMessage getMEssageFromBehaviour(ACLMessage msg1) {
        setReceivedmessage(msg1);
        //performs action message to exeute behaviour
        action();
        //returns ACL Message
        return getSendmessage();

    }

    /**
     * @return the receivedmessage
     */
    public ACLMessage getReceivedmessage() {
        return receivedmessage;
    }

    /**
     * @param receivedmessage the receivedmessage to set
     */
    public void setReceivedmessage(ACLMessage receivedmessage) {
        this.receivedmessage = receivedmessage;
    }

    @Override
    public boolean done() {
        return true;
    }

}
