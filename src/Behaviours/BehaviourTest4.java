/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Behaviours;

import jade.core.AID;
import jade.lang.acl.ACLMessage;

/**
 *
 * @author Diogo Martinho
 */
public class BehaviourTest4 extends jade.core.behaviours.Behaviour {

    /**
     * @return the sendmessage
     */
    public ACLMessage getSendmessage() {
        return sendmessage;
    }

    /**
     * @param sendmessage the sendmessage to set
     */
    public void setSendmessage(ACLMessage sendmessage) {
        this.sendmessage = sendmessage;
    }
    private String sendagentname;
    private String receiveagentname;
    private ACLMessage receivedmessage;
    private ACLMessage sendmessage;

    @Override
    public void action() {

        System.out.println(getReceiveagentname()+"  receiving message..." + getReceivedmessage().getContent());
        AID id = new AID();
        //sets ACL messages parameters
        setSendmessage(new ACLMessage(ACLMessage.INFORM));

        id.setName(getSendagentname());

        getSendmessage().addReceiver(id);

        getSendmessage().setContent("Hello from " + getReceiveagentname());

    }

    public void startChat(){
        
                System.out.println(getReceiveagentname() + " starting chat...");
        AID id = new AID();
        //sets ACL messages parameters
           setSendmessage(new ACLMessage(ACLMessage.INFORM));
        id.setName(getSendagentname());
             getSendmessage().addReceiver(id);
             getSendmessage().setContent("Start Chat");

    }
    public ACLMessage getMEssageFromBehaviour(ACLMessage msg1, String ReceiveAgentName, String SendAgentName) {
        setSendagentname(SendAgentName);
        setReceiveagentname(ReceiveAgentName);
        setReceivedmessage(msg1);
        //performs action message to exeute behaviour
        if(msg1!=null){
                    action();
        //returns ACL Message
        return getSendmessage();
        }else{
                    startChat();
        //returns ACL Message
        return getSendmessage();
        }


    }

    /**
     * @return the receivedmessage
     */
    public ACLMessage getReceivedmessage() {
        return receivedmessage;
    }

    /**
     * @param receivedmessage the receivedmessage to set
     */
    public void setReceivedmessage(ACLMessage receivedmessage) {
        this.receivedmessage = receivedmessage;
    }

    @Override
    public boolean done() {
        return true;
    }

    /**
     * @return the sendagentname
     */
    public String getSendagentname() {
        return sendagentname;
    }

    /**
     * @param sendagentname the sendagentname to set
     */
    public void setSendagentname(String sendagentname) {
        this.sendagentname = sendagentname;
    }

    /**
     * @return the receiveagentname
     */
    public String getReceiveagentname() {
        return receiveagentname;
    }

    /**
     * @param receiveagentname the receiveagentname to set
     */
    public void setReceiveagentname(String receiveagentname) {
        this.receiveagentname = receiveagentname;
    }

}
