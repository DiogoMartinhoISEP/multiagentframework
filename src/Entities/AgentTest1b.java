/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;


import jade.content.ContentElement;
import jade.content.ContentElementList;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Diogo Martinho
 */
public class AgentTest1b extends jade.core.Agent{
      protected void setup() {
          Object[] args = getArguments();
          int number = (int)args[0];
          System.out.println(getAID());
          if(number==1){
           
                   
                        ACLMessage msg1 = blockingReceive();
             if (msg1.getContent().equalsIgnoreCase("Start Chat")){
              addBehaviour(new SimpleBehaviour() {
                  @Override
                  public void action() {
                       System.out.println("Agent 1 sending message...");
                    AID id = new AID();
                    ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
                    id.setName("Agent2@192.168.3.110:1099/JADE");
                    msg.addReceiver(id);
                    msg.setContent("Start Chat");
                    send(msg);
                  }

                  @Override
                  public boolean done() {
                  return false;
                  }
              });
          }
                 
         }else if(number==2){
             ACLMessage msg1 = blockingReceive();
          if (msg1.getContent().equalsIgnoreCase("Start Chat")){
              addBehaviour(new SimpleBehaviour() {
                  @Override
                  public void action() {
                       System.out.println("Agent 2 sending message...");
                    AID id = new AID();
                    ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
                    id.setName("Agent1@192.168.3.110:1099/JADE");
                    msg.addReceiver(id);
                    msg.setContent("Start Chat");
                    send(msg);
                  }

                  @Override
                  public boolean done() {
                return false;
                  }
              });
          }    
         
          }else{
             
                     addBehaviour(new OneShotBehaviour() {
              @Override
              public void action() {
              
                  System.out.println("Agent 3 starting chat...");
                    AID id = new AID();
                    ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
                    id.setName("Agent2@192.168.3.110:1099/JADE");
                    msg.addReceiver(id);
                    msg.setContent("Start Chat");
                    send(msg);
                     id.setName("Agent2@192.168.3.110:1099/JADE");
                    
                
              }
          });
      }
          
     
      }
}
