/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import Behaviours.BehaviourTest1;
import Behaviours.BehaviourTest2;
import Behaviours.BehaviourTest3;
import jade.content.ContentElement;
import jade.content.ContentElementList;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Diogo Martinho
 * @see this class shows an example of Agent Behaviour Customization with
 * different external Behaviour Classes
 */
public class AgentTest3 extends jade.core.Agent {

    protected void setup() {
        Object[] args = getArguments();
        int number = (int) args[0];

        if (number == 1) {

            //Loads Behaviour1
            Behaviours.BehaviourTest1 b1 = new BehaviourTest1();
            //Gets ACL Message to send
            ACLMessage msg = b1.getMessageFromBehaviour();

            send(msg);
            addBehaviour(new CyclicBehaviour() {
                @Override
                public void action() {
                    ACLMessage msg1 = blockingReceive();
                    //Loads Behaviour2
                    Behaviours.BehaviourTest2 b2 = new BehaviourTest2();
                    //Gets ACL Message to send
                    ACLMessage msg2 = b2.getMEssageFromBehaviour(msg1);
                    send(msg2);
                    block();
                }
            });

        } else {
            addBehaviour(new CyclicBehaviour() {
                @Override
                public void action() {
                    ACLMessage msg1 = blockingReceive();
                    //Loads Behaviour3
                    Behaviours.BehaviourTest3 b3 = new BehaviourTest3();
                    //Gets ACL Message to send
                    ACLMessage msg2 = b3.getMEssageFromBehaviour(msg1);
                    send(msg2);
                    block();
                }
            });

        }

    }
}
