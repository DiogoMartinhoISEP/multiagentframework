/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import Behaviours.BehaviourTest1;
import Behaviours.BehaviourTest2;
import Behaviours.BehaviourTest3;
import Behaviours.BehaviourTest4;
import jade.content.ContentElement;
import jade.content.ContentElementList;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import sun.management.resources.agent;

/**
 *
 * @author Diogo Martinho
 * @see this class shows an example of Agent Behaviour Customization with
 * different external Behaviour Classes
 */
public class AgentTest4 extends jade.core.Agent {

    protected void setup() {
       
        Object[] args = getArguments();
        int number = (int) args[0];
//list of behaviours is parsed from the received arguments
List<BehaviourTest4> behaviours = (List<BehaviourTest4>) args[1];
        if (number == 1) {

            //Loads Behaviour 1
            addBehaviour(new OneShotBehaviour() {
                @Override
                public void action() {
                      ACLMessage msg = behaviours.get(0).getMEssageFromBehaviour(null,myAgent.getLocalName() , "Agent2@192.168.3.127:1099/JADE");

            send(msg);
                }
            });
     
            //Gets ACL Message to send
         
            addBehaviour(new CyclicBehaviour() {
                @Override
                public void action() {
                    ACLMessage msg1 = blockingReceive();
                    //Loads Behaviour 2
                             
                    //Gets ACL Message to send
                    ACLMessage msg2 = behaviours.get(1).getMEssageFromBehaviour(msg1,myAgent.getLocalName(),"Agent2@192.168.3.127:1099/JADE");
                    send(msg2);
                    block();
                }
            });

        } else {
               addBehaviour(new CyclicBehaviour() {
                @Override
                public void action() {
                    ACLMessage msg1 = blockingReceive();
                    //Loads Behaviour 3
                             
                    //Gets ACL Message to send
                    ACLMessage msg2 = behaviours.get(1).getMEssageFromBehaviour(msg1,myAgent.getLocalName(),"Agent1@192.168.3.127:1099/JADE");
                    send(msg2);
                    block();
                }
            });

        }

    }
}
