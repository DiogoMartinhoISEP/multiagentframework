/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gecad.agents;

import gecad.behaviours.FacilitatorAgentStartChatBehaviour;
import gecad.interfaces.FacilitatorAgent;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;

/**
 *
 * @author João Carneiro
 */
public class Facilitator extends Agent implements FacilitatorAgent {

    protected void setup() {

    
       FacilitatorAgentStartChatBehaviour fascb = new FacilitatorAgentStartChatBehaviour();
       ACLMessage msg1 = fascb.getMessageFromBehaviour("ParticipantAgent1@192.168.3.128:1099/JADE");
       send(msg1);
       ACLMessage msg2 = fascb.getMessageFromBehaviour("ParticipantAgent2@192.168.3.128:1099/JADE");
       send(msg2);
   
            
        addBehaviour(new CyclicBehaviour() {
            @Override
            public void action() {
                ACLMessage msg1 = blockingReceive();
                System.out.println("Sou o " + this.getAgent().getName() + " e recebia mensagem " + msg1.getContent() + " do " + msg1.getSender());
//                AID id = new AID();
//
//                ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
//
//                id.setName("Agent2@192.168.3.127:1099/JADE");
//
//                msg.addReceiver(id);
//
//                msg.setContent("Hello 1");
//
//                send(msg);
                block();
            }
        });

        
//        ACLMessage msg4 = blockingReceive();
//        
//        addBehaviour(new SimpleBehaviour() {
//            @Override
//            public void action() {
//
//                System.out.println("Sou o " + this.getAgent().getName() + " e recebia mensagem " + msg4.getContent() + " do " + msg4.getSender());
//
//                AID id = new AID();
//
//                ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
//
//                id.setName("Agent2@192.168.3.127:1099/JADE");
//
//                msg.addReceiver(id);
//
//                msg.setContent("Hello 1");
//
//                send(msg);
//            }
//
//            @Override
//            public boolean done() {
//                return true;
//            }
//        });

    }
}
