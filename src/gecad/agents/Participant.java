/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gecad.agents;

import gecad.behaviours.ParticipantAgentBehaviour;
import gecad.interfaces.ParticipantAgent;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

/**
 *
 * @author João Carneiro
 */
public class Participant extends Agent implements ParticipantAgent {

    protected void setup() {
    
        addBehaviour(new CyclicBehaviour() {
            @Override
            public void action() {
                ACLMessage msg1 = blockingReceive();
                System.out.println("Sou o " + this.getAgent().getName() + " e recebia mensagem " + msg1.getContent() + " do " + msg1.getSender());
                ParticipantAgentBehaviour pab = new ParticipantAgentBehaviour();
                ACLMessage msg = pab.getMessageFromBehaviour(msg1);
                send(msg);
                block();
            }
        });

    }

}
