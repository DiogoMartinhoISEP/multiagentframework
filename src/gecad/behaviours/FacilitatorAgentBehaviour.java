/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gecad.behaviours;

import jade.lang.acl.ACLMessage;

/**
 *
 * @author Diogo Martinho
 */
public class FacilitatorAgentBehaviour  extends jade.core.behaviours.OneShotBehaviour{

    /**
     * @return the msg
     */
    public ACLMessage getMsg() {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(ACLMessage msg) {
        this.msg = msg;
    }

    /**
     * @return the receivedmessage
     */
    public ACLMessage getReceivedmessage() {
        return receivedmessage;
    }

    /**
     * @param receivedmessage the receivedmessage to set
     */
    public void setReceivedmessage(ACLMessage receivedmessage) {
        this.receivedmessage = receivedmessage;
    }
     private ACLMessage msg;
 private ACLMessage receivedmessage;

    @Override
    public void action() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
