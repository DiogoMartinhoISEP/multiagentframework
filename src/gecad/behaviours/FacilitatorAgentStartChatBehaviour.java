/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gecad.behaviours;

import jade.core.AID;
import jade.lang.acl.ACLMessage;

/**
 *
 * @author Diogo Martinho
 */
public class FacilitatorAgentStartChatBehaviour extends jade.core.behaviours.OneShotBehaviour{
private ACLMessage msg;
private String agentadress;

      @Override
    public void action() {
                
        AID id = new AID();
        //sets ACL messages parameters
        setMsg(new ACLMessage(ACLMessage.INFORM));

        getMsg().setContent("Bem Vindo");

        id.setName(getAgentadress());

        getMsg().addReceiver(id);


    }
        public ACLMessage getMessageFromBehaviour(String agentadress) {
        setAgentadress(agentadress);
        //performs action message to exeute behaviour
        action();
        //returns ACL Message
        return getMsg();
    }


    /**
     * @return the msg
     */
    public ACLMessage getMsg() {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(ACLMessage msg) {
        this.msg = msg;
    }

    /**
     * @return the agentadress
     */
    public String getAgentadress() {
        return agentadress;
    }

    /**
     * @param agentadress the agentadress to set
     */
    public void setAgentadress(String agentadress) {
        this.agentadress = agentadress;
    }


    
}
