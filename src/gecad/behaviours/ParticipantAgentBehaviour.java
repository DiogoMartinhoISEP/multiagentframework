/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gecad.behaviours;

import jade.core.AID;
import jade.lang.acl.ACLMessage;

/**
 *
 * @author Diogo Martinho
 */
public class ParticipantAgentBehaviour extends jade.core.behaviours.OneShotBehaviour{
 private ACLMessage msg;
 private ACLMessage receivedmessage;
    @Override
    public void action() {
                
                AID id = new AID();

                setMsg(new ACLMessage(ACLMessage.INFORM));

                getMsg().setContent("Obrigado");

                id.setName(getReceivedmessage().getSender().getName());

                getMsg().addReceiver(id);

    }
        public ACLMessage getMessageFromBehaviour(ACLMessage receivedmessage) {
        setReceivedmessage(receivedmessage);
        //performs action message to exeute behaviour
        action();
        //returns ACL Message
        return getMsg();
    }

    /**
     * @return the msg
     */
    public ACLMessage getMsg() {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(ACLMessage msg) {
        this.msg = msg;
    }

    /**
     * @return the receivedmessage
     */
    public ACLMessage getReceivedmessage() {
        return receivedmessage;
    }

    /**
     * @param receivedmessage the receivedmessage to set
     */
    public void setReceivedmessage(ACLMessage receivedmessage) {
        this.receivedmessage = receivedmessage;
    }
}
