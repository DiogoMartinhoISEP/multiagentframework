/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mas4gdm;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Diogo Martinho
 */
public class MAS4GDM {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        jade.core.Runtime rt = jade.core.Runtime.instance();
        rt.setCloseVM(true);

        Profile pMain = new ProfileImpl();
        pMain.setParameter(Profile.MTPS, null);
        pMain.setParameter(Profile.LOCAL_HOST, "localhost");
        pMain.setParameter(Profile.LOCAL_PORT, "1099");

        System.out.println("Launching a whole in-process platform..." + pMain);

        AgentContainer mc = rt.createMainContainer(pMain);

        AgentController userAgt;
        Object[] participant1args = {1};
        Object[] participant2args = {2};
        Object[] participant3args = {3};
        try {
            userAgt = mc.createNewAgent("ParticipantAgent1", "gecad.agents.Participant", participant1args);
            userAgt.start();
            userAgt = mc.createNewAgent("ParticipantAgent2", "gecad.agents.Participant", participant2args);
            userAgt.start();
            userAgt = mc.createNewAgent("FacilitatorAgent1", "gecad.agents.Facilitator", participant3args);
            userAgt.start();
        } catch (StaleProxyException ex) {
            Logger.getLogger(MAS4GDM.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
