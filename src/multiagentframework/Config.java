/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multiagentframework;

import Behaviours.BehaviourTest4;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Diogo Martinho
 */
public class Config {

    public Config() {

    }

    public void createAgents() {

        jade.core.Runtime rt = jade.core.Runtime.instance();
        rt.setCloseVM(true);

        Profile pMain = new ProfileImpl();
        pMain.setParameter(Profile.MTPS, null);
        pMain.setParameter(Profile.LOCAL_HOST, "localhost");
        pMain.setParameter(Profile.LOCAL_PORT, "1099");

        System.out.println("Launching a whole in-process platform..." + pMain);

        AgentContainer mc = rt.createMainContainer(pMain);

        AgentController userAgt;
        Object[] participant1 = {1};
        Object[] participant2 = {2};
        Object[] participant3 = {3};
        try {
            userAgt = mc.createNewAgent("Agent1", "Entities.AgentTest1b", participant1);
            userAgt.start();
            userAgt = mc.createNewAgent("Agent2", "Entities.AgentTest1b", participant2);
            userAgt.start();
            userAgt = mc.createNewAgent("Agent3", "Entities.AgentTest1b", participant3);
            userAgt.start();
        } catch (StaleProxyException ex) {
            Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    

   //this method loads all the behaviours inside "Behaviours" package and sends it to the participant agent
   public void createAgentsAndLoadBehaviours() throws ClassNotFoundException, IOException {
   Helper.ClassLoader cl = new Helper.ClassLoader();
   //loads behaviours
   BehaviourTest4 bb4a = new BehaviourTest4();
      BehaviourTest4 bb4b = new BehaviourTest4();
         BehaviourTest4 bb4c = new BehaviourTest4();
   List<Behaviours.BehaviourTest4> behaviours = new ArrayList<>();
   behaviours.add(bb4a);
     behaviours.add(bb4b);
       behaviours.add(bb4c);
        jade.core.Runtime rt = jade.core.Runtime.instance();
        rt.setCloseVM(true);

        Profile pMain = new ProfileImpl();
        pMain.setParameter(Profile.MTPS, null);
        pMain.setParameter(Profile.LOCAL_HOST, "localhost");
        pMain.setParameter(Profile.LOCAL_PORT, "1099");

        System.out.println("Launching a whole in-process platform..." + pMain);

        AgentContainer mc = rt.createMainContainer(pMain);

        AgentController userAgt;
        //list of behaviours is sent to the participant agent
        Object[] participant1 = {1,behaviours};
        Object[] participant2 = {2,behaviours};
        try {
            userAgt = mc.createNewAgent("Agent1", "Entities.AgentTest4", participant1);
            userAgt.start();
            userAgt = mc.createNewAgent("Agent2", "Entities.AgentTest4", participant2);
            userAgt.start();
        } catch (StaleProxyException ex) {
            Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
        }
     }
}
