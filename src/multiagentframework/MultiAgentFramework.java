/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multiagentframework;

import java.io.IOException;

/**
 *
 * @author Diogo Martinho
 */
public class MultiAgentFramework {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException, IOException {
        Config config = new Config();
        config.createAgents();
        //config.createAgentsAndLoadBehaviours();
    }

}
